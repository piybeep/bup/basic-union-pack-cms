'use strict';

/**
 * do-point controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::do-point.do-point');
