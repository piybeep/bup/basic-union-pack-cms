'use strict';

/**
 * do-point router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::do-point.do-point');
