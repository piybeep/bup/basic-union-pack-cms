'use strict';

/**
 * do-point service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::do-point.do-point');
